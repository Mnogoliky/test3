import {
	createStore,
	combineReducers,
	applyMiddleware,
	compose,
} from 'redux';
import createSagaMiddleware from 'redux-saga';

import { rootSaga } from './rootSaga';

import videos from './videos/reducer.js';

const rootReducers = combineReducers({
	videos
});

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducers, compose(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);
window.store = store;