import { all } from 'redux-saga/dist/redux-saga-effects-npm-proxy.esm';
import { watchAllVideos } from './videos/saga';

export function* rootSaga() {
	yield all([
		watchAllVideos(),
	]);
}
