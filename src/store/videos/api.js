import { objectToBody } from '../../utils/functions';

const globalParameters = {
	key: process.env.REACT_APP_YOUTUBE_TOKEN,
	maxResults: 10,
	regionCode: 'UA',
	type: 'video',
	part: 'snippet',
};

export const getVideosBySearch = q => {

	const QueryStringParameters = {
		...globalParameters,
		q,
	};

	return fetch(`https://www.googleapis.com/youtube/v3/search?${objectToBody(QueryStringParameters)}`);
};

export const getVideosFromPage = pageToken => {

	const QueryStringParameters = {
		...globalParameters,
		pageToken: pageToken,
	};

	return fetch(`https://www.googleapis.com/youtube/v3/search?${objectToBody(QueryStringParameters)}`);
};