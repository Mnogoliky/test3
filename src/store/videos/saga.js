import { takeEvery, takeLatest, put, all, call, select } from 'redux-saga/effects';
import * as actions from './actions';
import * as api from './api';
import { setSearchLoadStatus } from './actions';
import { setPaginationLoadStatus } from './actions';

function* getVideosBySearch({payload}) {
	try{
		yield put(setSearchLoadStatus(true));
		const {q} = payload;
		const response = yield call(api.getVideosBySearch,q);
		if (response.ok)
		{
			const data = yield response.json();
			yield put(actions.setNextPageToken(data.nextPageToken));
			yield put(actions.setVideos(data.items));
			yield put(setSearchLoadStatus(false));
		}
		else
		{
			throw response.statusText;
		}
	}catch (e) {
		yield put(actions.setVideos([]));
		yield put(actions.setError(e.toString()));
		yield put(setSearchLoadStatus(false));
	}
}

function* getVideosFromNextPage() {
	try{
		yield put(setPaginationLoadStatus(true));
		const nextPageToken = yield select(state => state.videos.nextPageToken);
		const response = yield call(api.getVideosFromPage,nextPageToken);
		if (response.ok)
		{
			const data = yield response.json();
			yield put(actions.setNextPageToken(data.nextPageToken));
			yield put(actions.addVideos(data.items));
			yield put(setPaginationLoadStatus(false));
		}
		else
		{
			throw response.statusText;
		}
	}catch (e) {
		yield put(actions.setVideos([]));
		yield put(actions.setError(e.toString()));
		yield put(setPaginationLoadStatus(false));
	}
}

export function* watchAllVideos() {
	yield all([
		takeLatest(actions.getVideosBySearch.toString(),getVideosBySearch),
		takeEvery(actions.getVideosFromNextPage.toString(),getVideosFromNextPage),
	]);
}
