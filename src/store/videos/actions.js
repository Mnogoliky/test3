import { createAction } from 'redux-actions-helpers/lib/index';

const family = '@@videos/';

export const getVideosBySearch = createAction(family+'GET_VIDEOS_BY_SEARCH', q => ({payload: {q}}));
export const setVideos = createAction(family+'SET_VIDEOS', list => ({payload: {list}}));
export const getVideosFromNextPage = createAction(family+'GET_VIDEOS_FROM_NEXT_PAGE');
export const addVideos = createAction(family+'ADD_VIDEOS', list => ({payload: {list}}));

export const setNextPageToken = createAction(family+'SET_NEXT_PAGE_TOKEN', token => ({payload: {token}}));

export const setError = createAction(family+'SET_ERROR', error => ({payload: {error}}));

export const setSearchLoadStatus = createAction(family+'SET_SEARCH_LOAD_STATUS', status => ({payload: {status}}));
export const setPaginationLoadStatus = createAction(family+'SET_PAGINATION_LOAD_STATUS', status => ({payload: {status}}));
