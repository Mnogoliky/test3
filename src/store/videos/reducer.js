import { handleActions } from 'redux-actions-helpers/lib/index';
import * as actions from './actions.js';

export const initialState = {
	list: [],
	error: '',
	nextPageToken: '',
	searchLoadingStatus: false,
	paginationLoadingStatus: false,
};

export default handleActions({
	[actions.setVideos]: (state, {payload}) => {
		return {
			...state,
			list: payload.list
		};
	},
	[actions.addVideos]: (state, {payload}) => {
		return {
			...state,
			list: [...state.list,...payload.list]
		};
	},
	[actions.setError]: (state, {payload}) => {
		return {
			...state,
			error: payload.error
		};
	},
	[actions.setNextPageToken]: (state, {payload}) => {
		return {
			...state,
			nextPageToken: payload.token,
		};
	},
	[actions.setSearchLoadStatus]: (state, {payload}) => {
		return {
			...state,
			searchLoadingStatus: payload.status,
		};
	},
	[actions.setPaginationLoadStatus]: (state, {payload}) => {
		return {
			...state,
			paginationLoadingStatus: payload.status,
		};
	},
}, {initialState});
