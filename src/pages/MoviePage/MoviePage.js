import React from 'react';
import { BackButton } from '../../components/BackButton/BackButton';

export const MoviePage = props => {

	console.log(props);

	const { movieId } = props.match.params;

	return (
		<>
			<BackButton/>
			<iframe
				width = "560"
				height = "315"
				style={{margin: 'auto'}}
				src = {`https://www.youtube.com/embed/${movieId}`}
				frameBorder = "0"
				allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowFullScreen
			/>
		</>
	)
};