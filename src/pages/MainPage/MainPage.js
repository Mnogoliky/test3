import React from 'react';
import {
	Content,
	SearchBar,
	Pagination,
} from '../../components';

export const MainPage = props => {

	return (
		<>
			<SearchBar/>
			<Content/>
			<Pagination/>
		</>
	)
};