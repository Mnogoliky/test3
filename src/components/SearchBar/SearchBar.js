import React, {useState} from 'react';
import {connect} from 'react-redux';
import { Input, Button, Icon } from 'semantic-ui-react';
import './styles.css';
import { getVideosBySearch } from '../../store/videos/actions';

export const SearchBarContainer = props => {

	const {
		dispatch
	} = props;

	const [searchText,setSearchText] = useState('');

	const search = () =>  {
		dispatch(getVideosBySearch(searchText));
	};

	return (
		<div className="search-bar">
			<Input value = {searchText} onChange = {e => setSearchText(e.target.value)}/>
			<Button
				onClick={search}
				icon
			>
				Search
				<Icon name = "search"/>
			</Button>
		</div>
	)
};

export const SearchBar = connect(state => ({

}))(SearchBarContainer);