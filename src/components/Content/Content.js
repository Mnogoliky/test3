import React from 'react';
import { Loader, Dimmer, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { Item } from '../Item/Item';
import './styles.css';

const ContentContainer = props => {

	const {
		list,
		loadingStatus,
	} = props;

	return (
			<Segment className="videos-list">
				<Dimmer inverted active={loadingStatus}>
					<Loader inverted>Loading</Loader>
				</Dimmer>
				{
					list.map(item => <Item key = {item.id.videoId} data={item}/>)
				}
			</Segment>
	)
};

export const Content = connect(state => ({
	loadingStatus: state.videos.searchLoadingStatus,
	list: state.videos.list,
}))(ContentContainer);