import React from 'react';
import {Button, Icon} from 'semantic-ui-react';
import {withRouter} from 'react-router';

const BackButtonContainer = props => {

	const { goBack } = props.history;

	return (
		<Button onClick={goBack} icon labelPosition = 'left'>
			<Icon name = 'arrow left'/>
			Back
		</Button>
	)
};

export const BackButton = withRouter(BackButtonContainer);