import React from 'react';
import {Button} from 'semantic-ui-react';
import {connect} from 'react-redux';
import { getVideosFromNextPage } from '../../store/videos/actions';
import './styles.css';

const PaginationContainer = props => {

	const {
		nextPageToken,
		listLength,
		loadStatus,
		dispatch,
	} = props;

	if (listLength || nextPageToken)
		return (
			<div className="pagination">
				<Button
					loading = { loadStatus }
					onClick = { () => dispatch(getVideosFromNextPage()) }
				>
					next 10
				</Button>
			</div>
		);
	else
		return null;
};


export const Pagination = connect(state => ({
	nextPageToken: state.videos.nextPageToken,
	listLength: state.videos.list.length,
	loadStatus: state.videos.paginationLoadingStatus,
}))(PaginationContainer);