import React from 'react';
import { Card,Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import './styles.css';

export const Item = props => {

	const {
		id: {
			videoId
		},
		snippet: {
			thumbnails:
			{
				medium
			},
			title,
			channelTitle,
		}
	} = props.data;

	return (
		<Link to = {'/movie/'+videoId}>
			<Card className="item">
				<Image
					src={medium.url}
					wrapped ui={false}
					alt = "preview"
					className="item_preview"
				/>
				<Card.Content>
					{/*<Card.Header>Daniel</Card.Header>*/}
					<Card.Meta>{channelTitle}</Card.Meta>
					<Card.Description>
						{title}
					</Card.Description>
				</Card.Content>
			</Card>
		</Link>
	)
};