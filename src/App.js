import React from 'react';
import {Switch, Route, Redirect} from 'react-router'
import { MainPage, MoviePage } from './pages';
import './styles.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/movie/:movieId" component={MoviePage}/>
        <Route path="/main" component={MainPage}/>
        <Redirect to = "/main"/>
      </Switch>
    </div>
  );
}

export default App;
