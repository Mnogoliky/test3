# README #

## Used libraries: ##
- [`semantic-ui-react`](https://www.npmjs.com/package/semantic-ui-react) [`semantic-ui-css`](https://www.npmjs.com/package/semantic-ui-css)
- [`redux`](https://www.npmjs.com/package/redux)
- [`react-redux`](https://www.npmjs.com/package/react-redux)
- [`redux-actions-helpers`](https://www.npmjs.com/package/redux-actions-helpers)
- [`redux-saga`](https://www.npmjs.com/package/redux-saga)
- [`react-router`](https://www.npmjs.com/package/react-router)
- [`react-router-dom`](https://www.npmjs.com/package/react-router-dom)